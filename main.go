package main

import (
	"bytes"
	"crypto/md5"
	"crypto/rand"
	"encoding/hex"
	"fmt"
	"io/ioutil"
	r "math/rand"
	"os"
	"os/exec"
	"runtime"
	"strings"
	"time"
	"errors"
	"path/filepath"
	"io"
)

func main() {

	fmt.Println("run")

	argNum := len(os.Args)

	if argNum == 2 {
		secondArg := os.Args[1]
		switch secondArg {
		case "build":
			buildProject()
		}

	}

	if argNum >= 3 {
		secondArg := os.Args[1]
		inputArg := os.Args[2]
		switch secondArg {
		case "create":
			createProject(inputArg)
		}

	}
}

func buildProject() {
	fmt.Println("build all")

	if runtime.GOOS == "windows" {
		buildProjectWithWindows()
	}else{
		buildProjectWithLinux()
	}

	//移动静态文件
	CopyPath("config"+dirDot(), "build"+dirDot()+"config")
	CopyPath("storage"+dirDot(), "build"+dirDot()+"storage")
	fmt.Println("build success")
}

// linux下编译打包
func buildProjectWithLinux(){
	pwd, _ := execShell("pwd")
	pwd = strings.Replace(pwd, " ", "", -1)
	pwd = strings.Replace(pwd, "\r\n", "", -1)
	pwdArr := explode("/", pwd)
	if len(pwdArr) == 0 {
		return
	}
	projectName := pwdArr[len(pwdArr)-1]
	execShell( fmt.Sprintf("go build -o build/%s", projectName))
}

// windows下编译打包
func buildProjectWithWindows() {
	pwd, _ := execShell("cd")
	pwd = strings.Replace(pwd, " ", "", -1)
	pwd = strings.Replace(pwd, "\r\n", "", -1)
	pwdArr := explode("\\", pwd)
	if len(pwdArr) == 0 {
		return
	}
	projectName := pwdArr[len(pwdArr)-1]

	execShell( fmt.Sprintf("go build -o build/%s.exe", projectName))
}

func createProject(projectName string) {
	fmt.Println("create project" + projectName)

	projectPath := getProjectPath(projectName)

	err := os.Mkdir(projectPath, os.ModePerm)
	if err != nil {
		fmt.Println(fmt.Sprintf("create project error:%v", err))
		return
	}
	fmt.Println("create directory [" + projectPath + "]")

	projectDir := []string{"config", "storage", "build","http" + dirDot() + "controller", "http" + dirDot() + "middleware"}
	for _, item := range projectDir {
		subDir := getProjectPath(projectName, item)
		fmt.Println("create directory [" + subDir + "]")

		err := os.MkdirAll(subDir, os.ModePerm)
		if err != nil {
			fmt.Println(fmt.Sprintf("create project dir error:%v", err))
		}
	}

	writeMainFile(projectName)
	writeStorage(projectName)
	writeBuildDir(projectName)
	writeConfigFile(projectName)
	writeRouteFile(projectName)
	writeIndexFile(projectName)
	writeMiddlewareFile(projectName)
	fmt.Println("create project success!")
}

func writeBuildDir(projectName string) {
	gitignoreTpl = strings.Replace(gitignoreTpl, "\n", "", 1)
	path := getProjectPath(projectName, "build", ".gitignore")
	fmt.Println(path)

	err := ioutil.WriteFile(path, []byte(gitignoreTpl), 0644)
	if err != nil {
		panic(err)
	}
	fmt.Println("create file [" + path + "]")
}

func writeStorage(projectName string) {
	gitignoreTpl = strings.Replace(gitignoreTpl, "\n", "", 1)
	path := getProjectPath(projectName, "storage", ".gitignore")

	err := ioutil.WriteFile(path, []byte(gitignoreTpl), 0644)
	if err != nil {
		panic(err)
	}
	fmt.Println("create file [" + path + "]")
}

func writeMainFile(projectName string) {
	maingoTpl = strings.Replace(maingoTpl, "#projectname#", projectName, -1)
	maingoTpl = strings.Replace(maingoTpl, "\n", "", 1)
	path := getProjectPath(projectName, "main.go")

	err := ioutil.WriteFile(path, []byte(maingoTpl), 0644)
	if err != nil {
		panic(err)
	}
	fmt.Println("create file [" + path + "]")
}

func writeConfigFile(projectName string) {
	configTpl = strings.Replace(configTpl, "#appkey#", getRandAppKey(), -1)
	configTpl = strings.Replace(configTpl, "\n", "", 1)
	configPath := getProjectPath(projectName, "config", "config.toml")

	err := ioutil.WriteFile(configPath, []byte(configTpl), 0644)
	if err != nil {
		panic(err)
	}
	fmt.Println("create file [" + configPath + "]")
}

func writeRouteFile(projectName string) {
	routesTpl = strings.Replace(routesTpl, "#projectname#", projectName, -1)
	routesTpl = strings.Replace(routesTpl, "\n", "", 1)
	path := getProjectPath(projectName, "http", "routes.go")

	err := ioutil.WriteFile(path, []byte(routesTpl), 0644)
	if err != nil {
		panic(err)
	}
	fmt.Println("create file [" + path + "]")
}

func writeIndexFile(projectName string) {
	indexgoTpl = strings.Replace(indexgoTpl, "#indexHtml#", fmt.Sprintf("`%s`", indexHtml), 1)
	indexgoTpl = strings.Replace(indexgoTpl, "\n", "", 1)

	path := getProjectPath(projectName, "http", "controller", "index.go")

	err := ioutil.WriteFile(path, []byte(indexgoTpl), 0644)
	if err != nil {
		panic(err)
	}
	fmt.Println("create file [" + path + "]")
}

func writeMiddlewareFile(projectName string) {
	middlewareTpl = strings.Replace(middlewareTpl, "\n", "", 1)
	path := getProjectPath(projectName, "http", "middleware", "auth.go")

	err := ioutil.WriteFile(path, []byte(middlewareTpl), 0644)
	if err != nil {
		panic(err)
	}
	fmt.Println("create file [" + path + "]")
}

func dirDot() string {
	if runtime.GOOS == "windows" {
		return "\\"
	} else {
		return "/"
	}
}



//拷贝文件
func CopyFile(src, dst string) bool {
	if len(src) == 0 || len(dst) == 0 {
		return false
	}
	srcFile, e := os.OpenFile(src, os.O_RDONLY, os.ModePerm)
	if e != nil {
		panic(e)
	}
	defer srcFile.Close()

	dst = strings.Replace(dst, "\\", "/", -1)
	dstPathArr := strings.Split(dst, "/")
	dstPathArr = dstPathArr[0 : len(dstPathArr)-1]
	dstPath := strings.Join(dstPathArr, "/")

	dstFileInfo := GetFileInfo(dstPath)
	if dstFileInfo == nil {
		if e := os.MkdirAll(dstPath, os.ModePerm); e != nil {
			panic(e)
		}
	}
	//这里要把O_TRUNC 加上，否则会出现新旧文件内容出现重叠现象
	dstFile, e := os.OpenFile(dst, os.O_CREATE|os.O_TRUNC|os.O_RDWR, os.ModePerm)
	if e != nil {
		panic(e)
	}
	defer dstFile.Close()
	//fileInfo, e := srcFile.Stat()
	//fileInfo.Size() > 1024
	//byteBuffer := make([]byte, 10)
	if _, e := io.Copy(dstFile, srcFile); e != nil {
		panic(e)
	} else {
		return true
	}

}

//拷贝目录
func CopyPath(src, dst string) bool {
	srcFileInfo := GetFileInfo(src)
	if srcFileInfo == nil || !srcFileInfo.IsDir() {
		return false
	}
	err := filepath.Walk(src, func(path string, info os.FileInfo, err error) error {
		if err != nil {
			panic(err)
		}
		relationPath := strings.Replace(path, src, dirDot(), -1)
		dstPath := strings.TrimRight(strings.TrimRight(dst, "/"), "\\") + relationPath
		if !info.IsDir() {
			if CopyFile(path, dstPath) {
				return nil
			} else {
				return errors.New(path + " copy fail")
			}
		} else {
			if _, err := os.Stat(dstPath); err != nil {
				if os.IsNotExist(err) {
					if err := os.MkdirAll(dstPath, os.ModePerm); err != nil {
						panic(err)
						return err
					} else {
						return nil
					}
				} else {
					panic(err)
					return err
				}
			} else {
				return nil
			}
		}
	})

	if err != nil {
		panic(err)
	}
	return true

}

//判断文件或目录是否存在
func GetFileInfo(src string) os.FileInfo {
	if fileInfo, e := os.Stat(src); e != nil {
		if os.IsNotExist(e) {
			return nil
		}
		return nil
	} else {
		return fileInfo
	}
}

func getProjectPath(subdirs ...string) string {
	basepath := getGoPath()

	for _, item := range subdirs {
		basepath = basepath + dirDot() + item

	}

	return basepath
}

func getGoPath() string {
	stdout, _ := execShell("go env GOPATH")
	stdout = strings.Replace(stdout, "\n", "", -1)
	return stdout + dirDot() + "src"
}

func execShell(shell string) (stdout string, stderr string) {
	var cmd *exec.Cmd
	if runtime.GOOS == "windows" {
		cmd = exec.Command("cmd", "/C", shell)
	} else {
		cmd = exec.Command("/bin/bash", "-c", shell)
	}

	var out bytes.Buffer
	var stderrBuf bytes.Buffer
	cmd.Stdout = &out
	cmd.Stderr = &stderrBuf
	err := cmd.Run()
	if err != nil {
		fmt.Println("shell exec have an error", "err", err)
	}

	return out.String(), stderrBuf.String()
}

func getRandAppKey() string {
	randByte := RandomCreateBytes(24)
	h := md5.New()
	h.Write(randByte)
	return hex.EncodeToString(h.Sum(nil))
}

var alphaNum = []byte(`0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz`)

// RandomCreateBytes generate random []byte by specify chars.
func RandomCreateBytes(n int, alphabets ...byte) []byte {
	if len(alphabets) == 0 {
		alphabets = alphaNum
	}
	var bytes = make([]byte, n)
	var randBy bool
	if num, err := rand.Read(bytes); num != n || err != nil {
		r.Seed(time.Now().UnixNano())
		randBy = true
	}
	for i, b := range bytes {
		if randBy {
			bytes[i] = alphabets[r.Intn(len(alphabets))]
		} else {
			bytes[i] = alphabets[b%byte(len(alphabets))]
		}
	}
	return bytes
}

// 将字符串按字符拆成数组
func explode(delimiter, datastr string) (arr []string) {
	ret := strings.Split(datastr, delimiter)

	for _, item := range ret {
		if item != "" {
			arr = append(arr, item)
		}
	}

	return arr
}

var maingoTpl = `
package main

import (
	"gitee.com/zhucheer/orange/app"
	"#projectname#/http"
)

func main() {

	router := &http.Route{}
	app.AppStart(router)

}
`

var middlewareTpl = `
package middleware

import (
	"errors"
	"gitee.com/zhucheer/orange/app"
)

type Auth struct {
}

func NewAuth() *Auth {
	return &Auth{}
}

// Func implements Middleware interface.
func (w Auth) Func() app.MiddlewareFunc {
	return func(next app.HandlerFunc) app.HandlerFunc {
		return func(c *app.Context) error {

			// 中间件处理逻辑
			if c.Request().Header.Get("auth") == "" {
				c.ResponseWrite([]byte("auth middleware break"))
				return errors.New("auth middleware break")
			}

			return next(c)
		}
	}
}
`

var indexgoTpl = `
package controller

import (
	"gitee.com/zhucheer/orange/app"
)

func Welcome(c *app.Context) error {

	return c.ToString(#indexHtml#)
}

func AuthCheck(c *app.Context) error {

	return c.ToJson(map[string]interface{}{
		"auth": "auth is ok",
	})
}
`

var routesTpl = `
package http

import (
	"gitee.com/zhucheer/orange/app"
	"#projectname#/http/controller"
	"#projectname#/http/middleware"
)

type Route struct {
}

func (s *Route) ServeMux() {
	commonGp := app.NewRouter("")
	{
		commonGp.GET("/", controller.Welcome)
		
		commonGp.GET("/api", func(ctx *app.Context) error {
			return ctx.ToJson(map[string]interface{}{"data": "orange framework"})
		})
	}

	authGp := app.NewRouter("/auth", middleware.NewAuth())
	{
		authGp.ALL("/info", controller.AuthCheck)
	}

}
`

var configTpl = `
[app]
    name = "orange"
    key = "#appkey#"
    httpAddr = "127.0.0.1"
    httpPort = 8088
    maxBody = 2096157
    csrfVerify = true
    maxWaitSecond=120
    [app.logger]
        level = "INFO"
        type = "text"
        path = ""
        syncInterval = 2
    [app.session]
        isOpen = true
        timeout = 1800
    [app.upload]
        storage = "./storage/allimg"
        maxSize = 2096157
        ext = ["jpg","jpeg","gif","png"]
`
var gitignoreTpl = `*
!.gitignore
`


var indexHtml = `<html><head><title>Orange Web Framework</title><style type="text/css">p,form,ol,ul,li,dl,dt,dd,h3{margin:0;padding:0;list-style:none}.logo{	width:397px;	margin:0 auto;	}.menu{ height:50px; width:200px; margin:0 auto; margin-bottom:50px;}.menu li{ font-size:25px; line-height:50px; width:80px; text-align:center; float:left;}.menu li a{color:#777; text-decoration:none;}.menu li > a:hover{color:#777; text-decoration:underline;}</style></head><body><div class='logo'><img src='data:image/jpg;base64,/9j/4AAQSkZJRgABAgAAZABkAAD/7AARRHVja3kAAQAEAAAAPQAA/+4ADkFkb2JlAGTAAAAAAf/bAIQABgQEBAUEBgUFBgkGBQYJCggGBggKDAoKCgoKDA8MDAwMDAwPDA4ODw4ODBISFBQSEhoaGhoaHh4eHh4eHh4eHgEHBwcMCwwXDw8XGhQRFBoeHh4eHh4eHh4eHh4eHh4eHh4eHh4eHh4eHh4eHh4eHh4eHh4eHh4eHh4eHh4eHh4e/8AAEQgB7QGNAwERAAIRAQMRAf/EAMgAAQACAwEBAQAAAAAAAAAAAAAFBgMEBwIBCAEBAAMBAQEBAAAAAAAAAAAAAAQFBgMCAQcQAAEDAwICBgUHBgkKAwkAAAEAAgMRBAUSBiExQVFhIhMHcYGRMhShscHRQlIjYoJzsxU2cpKyM0NTJDUX8OGiY4OTozR0JcJUFvHSw9OEtFVWNxEBAAIBAwICBgcFBwMEAwEAAAECAxEEBSExQRJRYbHBIhNxgZGh0TIG8EJSFDTh8WJyIzMVkrIWgqLSU8IlNeL/2gAMAwEAAhEDEQA/AP1SgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgINTL5KDGYy6yE/81axukcK0LtI4NFelx4Bcs+aMWObz2rD5M6Q4r/i5vLxD+PFTmGGFlKV9qxv/ADm59MfY4fMlvQedO5GU8a0tJQKVo2RhPr1kfIu9f1DnjvWs/b+L782U9ZeduLeP7bjZoT1wvZL/ACvDU3H+osc/mpMfR1/B6jLC143fe0sjQW+Sia8gfhzHwXV6qSaan0K1w8nt8na8fX09r3F4lPAgio4g8ipz0ICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICCk+b90+HZ7o28rm4iid6BWT54wqbnrzXbafxTEe/3OeTs4VK1xbqZ77eLe3s9axdZ9Lg+xStljD28j0HmCOBB9C+WjSdB6XwEE1h947lw4DbG/kZC3lA8+JEPQx+oD1KZt9/nw/ktOno7w+xaYdFwHnNYStZFm7d1tLydcwgvi9JZxe31aloNr+oKT0yxpPpjt+PtdYy+lfMRm8XmLX4rG3DbiGpa4tqC1w6HNIBHrCvcG4x5q+ak6w6RMS3V2fRAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQUnzgtnTbQMgr/Z7iKQ06jqj4/x1S89TXba+i0fh73PJ2cNWKcGlM74O58Y/8tOQJfyH8g/0HkV2rHnjTxgbq4ggICDZx+Sv8dcturG4fbTt5SRuLTTqPWOwrpizXx281J0kidHVdn+blvcNbabhcILitGXrW0jd+kA9w9oGn0LUbDna2+HN0n0+H1/to7Vyel0iOSOWNskbg+N4DmPaQQQeRBC0UTExrDq9L6CAgICAgIKfuvzO2/gxLBG43t+wOrDDQtY4fffy9Tanroqrd8vhwz5Y+K3q98vFrxCE215s3WV3LBY3VtFa2N0TFCGlzniR3uanmgNT3eDRxKgbPnLZc8UtEVrb2+DzXJrLpa0jqICAgICAgICAgICAgICAgICAgICAgICAgICAgICAg0c7iocth7vHS+7cxlgJ6Hc2O/NcAVw3OCM2O1J8YfJjWH5pnhlgmkhmaWSxOLJGHmHNNCCvzm1ZrMxPeEVhliZLG6OQamPBDgeor5EzE6wNKwnfDK7H3BJkjFYJD/SR9Hrb0rtkrEx54Jb64AgICAgteyd/X23J/Cl13OLf/OW2riwk+/HXgD2dKtOO5O+2nSetPR+D1W+jtuF3BiM1ai4x1yyZpAL4waSMr0PZzaVs9vuseautJ1SImJSCkPogICAg5L5h+Zkssr8TgZ9EDCW3V9GSHPcDxZE4cmjpcPe6OHPK8rzEzPy8U9PG34er1+P0d+N7+EOV3jtMI5d58beP5TwPpWdpHVyb1hO63vredvvQyskHpa4Hp9C+4reW8T6JIfqBfpaWICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIOMebu2H2WXGZgZ/ZL6gmIHBk4FD/HAr6arH87s5pk+bH5bd/p/tcMleurnyoHNp5OydcQh8R0XMJ1wP7R0egrriyeWevaSJfcbkG3kFSNEzO7NGebXBMuPyT6iYba5AgICAg38Jmr/DZKLIWL9E8R5H3XNPNrh0grvt9xfDeL17wROjvO0N5Y7cll4kP4V5EB8TaONXMP3h95p61udhyFNzXWOlo7x+3gk1tqsCnvQgIOd+au9n4+A4PHvpeXLK3coPGKJ32B+U8ewekEZ/m+R+XHyqfmnv6o/t9jlktp0cbWQcWtemstrHw781SD1Ma53D1gLpj7TPqG01rnODWiriaAdpXiIH6kt2vbbxNkNXtY0PPaBxX6ZWJiI1S3tegQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBBpZnE2mXxlxjrtuqG4aWk9LTza5va08QuO4wVzUmlu0vkxrD865/BX+DycthesLXxk6JKHTIyvdew9IK/Pt1tr4LzS39/rRpjSUco74h8rbzWdwMpaCpHC5jHJzetS8NovHkt9T7CTtLqG6gbPCasd7QekFR70ms6S+Mq8AgICAg28XlL7F30V9YymG5iNWuHT1gjpB6QuuHNfFeLUnSYInR+g9p7nstw4qO8gcBO0Bt1AOcclOI9B6Ct9sd5XcY4tHfxj0Sk1trCaUx6R+fzllhMVPkbt1I4h3GD3nvPusb2uKj7rc1wY5vbwfJnSH5vyF9c397Pe3Ty+4uHukkcetxrw7B0BfnmXJbJabW7yizOrXXMaUrg/MQR9MUMkh/OLWj5iu0RpjmfWeCZxFrNd5WztYTSWeeKNhpWhc4AGnHkmCk3yVrHeZgju/Ti/SUsQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEFZ33s2HcmMDWER5C2q60lPIk82O/JdT1Kt5Pj43NOn547fg8Xrq4Hd2lzZ3MtrcxuiuIXFkkbhQtIWFyUtS01tGkwjzDCQCCCKg8CF4FcnbPgr3xogX4+c99n3T/lyU+sxmrpP5oeu6wQTxTxNlicHMeKghQrVms6S8va8ggICAglNvbiyeByDb2wk0v92WN3FkjelrgpO13d8F/NSf7X2LTD9A7b3BZZ7ExZC0NGv7ssZ5xyD3mH0V9i3u03Vdxji9f7pSazrDmnnPn/ABr+2wkTqstR49yBy8V47gP8FnH85Zz9QbrzXjFH7vWfp/u9rllnwc1WcchBG493j5K+uQSWMLYIz0dzi75SpGWPLSsfW+y6Z5Q4a4u9zDIjhb45jjI7rfKx0bWjp5Fx9XarPgdvN8/n8Ke/o9Y46u3LaJAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICCm+Yexoc9YvvLOMDMQN/DcKDxmj+jd2/dKp+V42M9PNWP9SPv9TxemrhksUsMr4pWOjljJa9jgQ5pHAgg8isTasxOk90dhuIIriF0MrdUbxQhfa2ms6wKzqvcBd6eMtlIeHb9TgrDSuevos9d1is762vIhJA8OHSOkHqIUC+OaTpLzozrwCAgICC5+Wu849v5CW3vSRjbwDxHc/DkaDpeB1HkfV1K44jkI295i35LfdL3S2irZTIT5HJXN/Oay3MjpXca01GoA7ByCrM2Wcl5vPe06vEzq1VyGrk71tlZSTn3gKRjreeS64sfntEEQxYO2NvjYg7+ckrJJ6X8fmovW4v5ryS/RHlft44nbUc0oIusjS4lBFC1pH4bf4vH1rZcNtfk4ImfzX6/gkY66Qt6tnsQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQVXeHl7idwtfcD+zZTTRl03k4jkJG/a6q81V7/AIrHuOv5b+n8Xi1IlwzKYu+xd9LY30RiuISQ5p5Hqc09LT0FYnNhvivNLRpMI8xo0Li3huIXRTND43cwV4raazrAq97iMhi5Tc2T3OhHHU33mjqcOkKxx56ZI0t3eonVt2G62Ooy9ZpP9aziPWFyybOe9Xyap6GeGdgfC9r2HpaaqHasxOkvj2vIICAgIPj3tY0veQ1rRVziaAAdJX2I1EDG45vIh9D+zrU1AP239v8Aly9KmzHyaf4rPvZ0zy32mc9mxJOP+32JZLc1AIea1ZFQ9D6GvYD2LvxOx/mMus/kr1n8PreqV1l3tbpIEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQRG5Nq4jcNp4F/FV7a+DcM4SRk/dd1dYPBRN3sse4rpePonxh5tWJcN3Xs3K7dvHR3DDLZuP4F41vceOo89LuwrFb7j8m2tpPWvhLhaswgFAeUXkNu2N3V7B4Ep+00cD6WqTi3Vq+uH2JQM2JzGNeZYtRaP6SIkig6x9am1zY8kaT977rEtmz3XcR0bdRiUdL2913s5Fc77OJ/LJ5U3aZzG3NAyYMefsP7p+Xn6lEvt718HnRvLgCAgr+Su5spdfs2yP4LTW4mHLh9A+UqdipGKvnt38H2Oiy7ewMt1c2uIxzKyzODGV5V5ue88eAHErjjpfPkisfmsREzL9FbT2vY7dxTLK378zu9dXJFHSv6+xo5NHR6albvY7Om2x+Wvfxn0pFa6QmVMehAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBBqX77J9u+G6YyWCQUkjkAc1w6iDUFcctqeWYt1h0pimzkW6djYo3D58NMIGmpNrJUsrX7DuYHYarIb3Y4vNrjnT1JH/E3tGtVGurK5tJPDnYWO6DwIPoI4FU9qzWdJV+fbZMM6XrMMC8uDUu8Vj7qpmhaXH7Y7rvaF1pmvXtJqh7naDTU209Opsg/wDEPqUqm99MPXmaos9x47+Z1ujHIMOtv8Xj8y6+fDk7nSWe33VcxvEd3ADxo4tq1w9RqvFtnWetZPK3MjfXF9K7H47j0XNx9lo5EVXHFjikee/1Q+RCSxmMhs4mwQN1PdTU+nee5ccuWbzrL7Wtr2itY1mVz28+fDv8eF1Ll4o9w6ueldNvmtinWvduNhwtMWL4+t7d/V6nSMBv2ObTDe91/IPWm2fMxbpdB3nDTXrRcYbiGZgfG4OaepXtLxaNYUN6TWdJZF7eBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBB8c9rRUr5M6PsRqhMtuG3tWkBwLupV+53taLDbbK11Iyu6pJXEB1ewclntzyM2aLbcdFYV+4yFxMe86g6gqu+a1lnTDWrVkYyQUkaHjqIquT1kxUvGloiY9bQnw1u+piJjd1cwvOij3X6dw36458k/bH7fW0ZcReM4tAkH5J4+w0XzRRZ+A3NO0Rf6P7dGs+2uGCr4nNHWWmiK3Js81PzUtH1SxkEcwvjhNZgNn8RwMHi06CzVT5F6iZjs6Y9vkv+Wsz9ENqHDXZrSIRAmprQVPXQJpMrLDwe6yfu+X6Z/aUtYY9tq0lxD5HfapyHUF9iGp4viq7WJmdLXnx07eptr6t30Eg1HNNRYdv7rurCRscji6HqPQrTZcjbFOk9lVveNrljWO7pOLzFvewtfG4Go4rW7fdVyRrDJ7ja2xzpKRBBFQpSIICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICDxLK2NpcTQDmV5tbSHqtdZUzcW7mRaooHceRIVBvuTiOlV/seMm3Wyi3mSuLl5LnGhWcy57XlpMWCtIaa4O4gICD6GuPIVX3R8mW3b4jIXFPChc6vYu1Ntkt2hwvucde8rJiNg3U4a+6qxv3elW224a1utuiq3PM0r0r1Wa22Jh4mgPj1nrJP0K3x8PhjvCnycxmntOjLLsjCvbQQhvoJXu3E4Z8HmvL5o8Vfy+wIImOkt5QynQ48FV7nhq1jWsrPa81a06WhTLyxntZCyQculUOXFak6Sv8Wat41hrrk6iCVwmcuMfO0hx8OvEKbtN3bFb1IW72lctfW6fh81DeQNe11a8wtftd1GSusMfutrOO2kpgEEVCnxKBMCAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIPEsrY2FzjQDiSV5taIjWXqtZmdIc/3Xu4uc62tXd0cHOHSsxyPJaz5atPxvGaR5rKTJK+Rxc41JWetaZ7tDWsRHR4Xx6EBBkigllcGsaXE9AXqtJmejza8VjqseJ2Tf3Za6UeGw9fNWu24nJk6z0hU7nlsePpHWVxxuysZaNDntD3DmXK+wcVjp3UOflsl+yV/7basoAKDqHD28lN/06Qhf6l5aNzuzF2/d8Rlfug6j/o8PlUa/JYq+MJOPjct/BGz72JB8CCR4+8QGD5aqJflv4Yn2JdOJ/imPai7vemQJ4GGP0uLz8hooeTlcnqj70zHxWP1z9yDvdzZGcmty78xoaFXZd/kt+8ssWwx1/dREk+slz6vcebnGqg2vr3Ta007MRXh0fEBBL4HNzWFwO8fDJ4hTdnu5xW9SDvNpGWvrdQxWUjuIWva6rXLY7fcReNYY7cbeaTpKVBBFQpqEICAgICAgICAgICAgICAgICAgICAgICAgICAgIBIAqUFD3rujTqsrZ3H+kcPmWb5XkNPgq0vE8fr8dlBe9znFzjUlZmZ1aaI0eV8fRAAqgmsLtq9yMgo0tj6XFT9rsL5Z9Sv3e/pij1ugYnbOMxrAZAHS9vErT7bYYsMde7Mbnf5c09OzZvtw4+yaWh7WkfZHF3sH0rtm3uPH4uOHZZMk9levt4XEgPgN0t/rJD8w5Krzcnafy/etcPGVj833K3e5szOrPM+d33QaNVTl3fmn4p8y2xbTyx8MeVHuyso4RNbGOsDj7Sos7mfDokxto8erXkup5DV7y70lcrZLT3l2rjrHaGIknpXjV70fEBB9aATx5JD5KQvcVLFbx3cbHfDSDuuPHsPJSsu2mtYvEfDKNi3MWtNJn4oRyipQgse188+1mEMrvw3cBXoVrx+8mlvLPZVchs4vXzR3dLsbtr2jjUHkVrcOXWGRzYtJbykowgICAgICAgICAgICAgICAgICAgICAgICAgICCv7uzzMbZOYx347xRo6lV8nvIw00jvK04zZzmvrPaHJ55nzSukeaucakrFXvNp1ltqUisaQxry9CD61pcaAVJX2IfJnRaNvbabLS4u+EbeNPrqrjZbDzfFfsp97v/L8NO6zzbhx9hD4NqA5w4UZ9Llb23uPFGlVPXZZMs62V3Ibkupa65fBjP2GcyqrPv7T3nSFrg2FY7RrKCmytT+E3j993EquvuPQsqbf0tKW4mlNXuJUe15nu71pEdmNeXsQEBAAqg2rPH3N3II4GOkd06RUBdsWC150rGrjlz1xxradF02/tRkYDprETTAirpXVaB6BQfOr/ZcdEfmprPrZ/e8jM/lvpHqWq9xDLuwdbzMY3hRrWDgArrLtovj8sxClxbmaZPNEy5PmsVNjrt8Tx3a90rFbvbTivpLbbTcxlprCPUVKfWkggjmEiSYXraGd8VgtpXd9vulaTjN55o8ss3yez8s+aF6tZw9tDzC0eO+sM3kppLOujmICAgICAgICAgICAgICAgICAgICAgICAgIMdzOyCB8rzRrASvGS8VrMy946Ta0RDj+5ctJkMhI8mrASGrC7/czlyTLd7DbRixxCHUFOEBBJYyONjhI5usjkDyUvBWInWUTcWmY0hIXuaeWhkklGjlEzgFJy7qdNJn6kXFtY11iPrRM2RldUR9xvyqFbPM9k6uCI7tRznONSanrXCZ1dojR8AJ5I+vbY3FfYq8zZ78Ajnw6yV68jz53hxYODeJ6yvk6PUavC8vT2yMu48h1r7FdXmbaLBgtrXOQIkeDDadMhHecOxWmz462XrPSqs3nI1xdI62dDxWGtrWBscEYjiHM0o53rWo2+1rSulY0hltzurXtradZSrWta0NaKAcgFNiNEKZ1fV9fFc3dgGX9m57G/isFQVVcls4y01jutuM3s4r6T2cpmifFI6N4o5poVi7Vms6S2lbRaNYeF5emeyupLa4bKw0IK6Ysk0trDnlxxeukupYPKNuraOZp40GoLY7PcResTDG7zbzS0xKxMeHtDh0q1idYVUxpL6vr4ICAgICAgICAgICAgICAgICAgICAgICAgqu/Mt8NY/DsdR8nP0Kl5jc+SnljxXXD7bz380+DlziSST0rHS2MQ+I+iD0wtBqfYvsPksr7uUt0tOlvUF7nJPZ4jHHdgqubo+hrjyCRD5Mti3sZpnaY2l56ach6SutMM2no5XzRWOqSbhvBZruHACldLeHtcfqUuNr5Y1siTuvNOlWCeWKJvcAjaeQHvH6aLne0V7dHWlZt36o+WZz+xvQAotrapVa6MfNeXpsW9pJLI2NjC+Rxo2NvEldaY5tOkdZcr5IrGs9IXbA7Na0smyAEknNluOLR6etaDZ8XppbJ1n0M9vOUmda4+keldbeyawNLgKjkwch9a0FMUR3Z++WZ7NldnEQEHxzQ5paeRXyY1fYnRzLfODNtcm5jb3H81keX2nkt5o7NfxG889fLPdUVRrwQWTaGWNvdCB57j+CtuM3Plt5Z7Knk9t5q+aO7pdhNXuV4Hktbhv4Mjmp4t1SEcQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEAmgqehByfe2RNzlHtB7rDQBYrls/nyy23E4PJihW1UrYQEBB6axzuQ4da+xGr5MxDds8TdXJHhxlwP2jwb/nUjFtrX7Qj5dzWneVkx20BQPuzX8ihA9g4lWuDjPG6pz8n4Ubl4bHHxaWAMpwAFK19A4VUjL5MUdEfF58s9VXyOUdI88anoaeNPT1lU2fcayucG30hFPe57i5xqTzKhzOqbEaDWFxoPakRqTOiWw2BvMjIGwNpH9udw4D0dam7XZ3yz8Pb0oO63lMUfF39DoWD23a2MdIG6pD79w4Ak+hajabGuOPh7+ll93v7ZZ+Lt6E/FCyMd0celx5lWdaxCsteZe16eRAQEAuDRUmg6ykyRGqG3DbW2QxkrGuDnMFVA3uOuXHMLDZZLYskS5DcxGKZ7D0EhYW9fLOjc47eaNWJeXtkgldFK17TQg1XqltJ1eb180aOo4HIfEWcUoPeaACtjs83npEsbvMPkvMLMx2pod1q3idYVExpL6vr4ICAgICAgICAgICAgICAgICAgICAgINfIzeDZTSctLSuWe3lpMuuCvmvEOK5KYzXcjzxq4lfn+e3mvMv0HBTy0iGquLsIFEGza2c00rY2ML5He6wfSuuPFNp0iOrlkyxWNZnouWI2WaNfdUdLzEY409XJX224rxt3UG55Xwr2WqDF21nFrko0AcAOZ7KlXVNvXHGsqW+4tknSEHm9wxQgxwDvH3Q36/pVdu97FelVjtNlNutlHv8AJSTSE6tTvvdA7GrO5s82lo8OCKwjiaqKlPbIy70fOvUV1eZtotmA2hJcBs96DHB9mHjqd6VdbPjJv8V+lfQpN7ycU+GnW3pX6yxkUMTWBgjiaKNjbw4dtFpsWCKxpppDM5dxNp111lvgACg4BSUYQEHh00TfeeB615m0R4vUVmfBikv7VnFz+HXyHtNF4tmrHi91w2nwRGQ3jjLYENla5w6AdX8n61Bzcpip4p2Hi8t/BU8pvi6uKtgBA+87gPUAqXccva/5V3t+IrT8yBdmbtzy6Sd7gTxa0kAqsndXmesys42tIjpENG4m8WQvpSvQo97eadUilfLGjEvD2ILlsm9PegJ9CvuJy/uqHlsX7zoVi/VDTqWowzrDLZo0lsLq5CAgICAgICAgICAgICAgICAgICAgICCH3XN4WGmPIkKByN/LhlP42nmzQ47IavJ7VhLd28rHR5Xx9EEvjMPLeNa2IDWTxceQU3b7WcnSO6DuN1GOdZ7OhYHbdtYRBrG6rj+lnPGh6gtRs9hXFGkfm8ZZfeb+2WdZ/L4QsMcTIxRop1npKtK1iOyrtaZ7qbufcXhl0bT3gS1rfRw49ioeQ32nRfcfsdeqhXl9JM53erqPed0n/Ms1lyzaWlxYYrDUXF3ZoIHSPa0Auc7g1o5le6U1lzveIhdtvbdtLbTc3pElxzZEOTVodlsaU+K/W3oZ7e769/hp0r6VrZk8Xbir5ma+0tAHYKlXUbjFTvKlnb5b9oYZd3YeMfz7K9jq/wAkFeLcnhjxh0rxmafCWnLvrHgfhhzz+Swn59Kj25fH4ex3rw+Tx9rSn3zMR+HbvHa4tYPpUe/Lz4RPsSKcRHjMe1HXG8Mg8nvRRDrLnSH2Dgot+TyT6I+9LpxmOPTP3I2fct04EOvJPRE0MCiX39p72n6uiXTYVjtWPr6oy4yokNaOeeuR5d9SiX3Ovr+mUum209X0Q033MjuoDqAUecky7xjiGIuJ5leNXvR8R9EBAQTO2JzFkGceBNFP4++mSEDkKebHLqmLfUO7eK2e3ljNxDfUlFEBAQEBAQEBAQEBAQEBAQEBAQEBAQEFf3vX9jPp/lyVXy3+zK04n/ehyR3vFYiW3h8R9fWirgEh8l0bZ9tG1kRp2+srVcZjiIhleTyTMyujWtaKNFAtBEaM/M6vM0nhxOf0gHSOs9AXy9tI1faV1nRx3cV06a+e540y1OsdtVg99km1+vdvNjjitI07IhQk59CDYhnji4iurrqQutbxVxvSbMvx7OkE+kk/SvfzoePky8m/HQxvs+slfPnep9+T63w5Gb7NG+gAfQvk55fYwQ8Ovrl3N5p6Svk5rT4vUYax4MTppHc3LxNpe4pEPJc49K86vuj4j6ICAg2Lewubg/hsNPvHgF1phtbtDlfNWneWa+x3wjGa3EyOFS2lKL3mwfLiNe7nhz/MmdOzRUdJEG/hnEX0R/KCk7WfjhG3UfBLrGEJdHXsW12k6wxO7jSUqpqEICAgICAgICAgICAgICAgICAgICAgIIfdkBlw01BUtFVA5Knmwyn8bfy5ocdkFHkLCW7t5Xs8r4+vTPfHpX2O75PZ0vazgLaJ3YFruOn4YZHkY+KVvV4omtkGSPtyI6h1ahw5jtXHNEzXo7YJiLdXINwtccnMSDUHS4kc3AcVht7H+rLdbKf9KEWoaYICAgIFEH3S7qK+6Pmr7of1JpJrDyQvj6IABPLigzwWk0sjY2ML5HcAxvErpTHNp0iNZc75IrGszpC14nY079Ml64NrxEDOJ9ZV1tuItPW/2KXc8vWOlPtW+x2zY27A57KU+yPpKvMOwpSOqizb+956Ofbxu4pco9kIDY4+6AOSzHKZItlmI7Q1HF45riiZ7yr6rFmIN/DNrex9hCk7WPjhG3U/BLrWBYRaBx6VttnHwMRvJ+NJqYhiAgICAgICAgICAgICAgICAgICAgICDDeQCe1liP22kLnlp5qzDpiv5bRLi+YtHW19JG4UoTRYDdY5peYfoG1yRekS0VHSAc0F/wBm3jZLTw695i03F5daaMzymLS+q8wPD4gfatHSdYZu8aSyL08ofMbbs76CSkbfHIOhxA5+migbrY0yVnp1T9rvr4rR16K1NsuECnwbuVKgEmvX3XFVNuKj+H9vtW9eVn+L9vsa52fb9NnOPQHfSuX/ABlf4bOv/KW/iq+t2fa/+TuT6vrK+xxdf4bPk8pb+KrJ/wCkrVgqbCan5RaPncvX/G1j9y33fi8f8laf36/f+DBcYnHWzdT7INPU+RoPsFSud9tjp3r97rTc5L9rfci55oRXwraKNv3iSfqUO948KxCZSk+NplFz3LQ40Ic7sFAod8iZTG1XyyP94+roXGbTLtFYh4Xl6ZGQuNK8K8h0leoo8TdPYna19eUc5vw8HS93M+hWW24++T/DCt3PI0x/4pXnC7ctbKP+zsAJ96Zwq4rR7XY1xx8MfWzm631sk/FP1J2KGOId0celx5lWVaRVW2vNkNunOx4+weA78V4o0Kv5DdxipPpWHHbOcuSPQ5JPM6aV0jjUuNSsRe02nWW4pWKxpDGvL0IJrbVuZLsEDpU/YU1ur9/fSjrdjD4NrGzqAqtxhr5axDD5rea0yyySxxirjTs6V7m0R3eYrM9kJkty29saNNSDxaDxNOtV2ff1osMGwtd4tN0NmLRKGQF57oc7iG9ZXzHyHm79NXrJx/l7azomra6iuGl8dS0ciRSqn48kXjWFfkxzSdJZl0cxAQEBAQEBAQEBAQEBAQEBAQEBBQN/YM6vi4m8/eosxzO06+eGn4bd9PJKhLNtKIJXb+VdYXjXE/hk0cFM2W5+Vf1IW923zaet1PF30UrGuY6rHioWz2+aLRrDG7jDNZ0nulFMQhAQfHvYxupxoOsr5MxHd9iJnsi8hn7S0YSXAHq5n2fWoefe0pCZg2V7yqmS3ZcSgiI+G377jx/zKlz8la3bousHG1r36qxdZYucTUyvP2jyVRk3Os+lcY9tpHoR8txLKavdUdXQotrzbulVpFezEvD29ticeJ4Beoq8zZL4zb19duHhxljOmRwU7b7K+TtCDuN9TH3nqueH2jaQUfp8aUc3u5Aq/wBrxlK9e8qDdcne3TtCzQWUcYGrvEdHQPUremKIU98sy2CQAujmi8vnLaxhc57hqA4NUPc7uuKNZTNttLZbaQ5ZnczPkrp0jz3fsjoAWN3m6tmtrLZ7Pa1w10hFqGmCABU0QXPaTbS1AnuHtYBx7xAV9xsUp8VpUHJTe/w1hP3u9LVoLbbVKR9wUH8Yq0y8rWOleqsxcVaetuiu5Dc97IHB0jYGnm1pq4+k81VZuQvPedFph4+kdo1V+fKPL9UZJd0Pd9AVXfcTr0WlNvGnVgZfXOsOLyXVrU8VzjNbXXV0nDXTs6BtLL5S6hYxketjeD5SKD1n6lp+N3OW8REQzHJ7bFSZmZXMGoqFfqAQEBAQEBAQEBAQEBAQEBAQEBAQa9/Zx3du6J4qCFyzYovXSXXDlmltYcl3LgpcdduIafBcatKxO/2c4b+pt9hvIzU9aFVesBBN4Lclzj3hrjqi6irDZ7+2KdPBXbzYVyxr4uj4bcVlextDZAHfdK1e131Mkd2U3WxvjnsmQ5pFQeHWp+qBo0r3LW1swkuHDpPL1da4ZdzWkJGLbWvKo5XdkkmpsJIHS8/5cFRbnkpnpVebbjYjrZVLvLFzjQ+I77x5KlybnWfSuse209SOlnllNXur2dCi2vM90utIjsxry9MjYnHnwr7V6irzNkpj8Dd3DhRvht6Xu5+xTMOztf1IWbeUp61sxO18bCQ+RwllHMk8AfSrvbcfjr1nrKk3PIZLdI6Qs9vFYxNAdI0gcmjg0K3pWlfFUXte3aGd2SsYxQyAAeofKus56R4uUYLz4NO43RiYRxnYT/Cb9aj35DFXxSKcflt4SgMpvy1a0ttyHO6+P1Kt3HMViPhWW34a2vxKRk8xc30hc9xoehZ3cbq2SerRbfa1xx0R6jJQgIPTKA1PJfYfJbbb9rGgMjbUfaIqflXaM2naHCcOveWOS/uX8NZA6hw+ZebZrT4vdcNY8GuXE8zVctXTQAJ5cUfWza2j5ZWxtaZJHGjY2CpK7Y8c2nSOsuOTJFY17Q6dtPD3trZht64Mae9HbtPIdOqi1/G7W9KaX+xj+S3VL31p9qyAAAACgHIK2hUyICAgICAgICAgICAgICAgICAgICCOzGIt8hbujkaDUcCom621ctdJS9rurYraw5VncBdYydwc0mInuuWM3mytht6m02e9rmr60SoSaIM9teT27w6JxaQumPLak6w55MVbxpKftt75GOLQ46h2qzpy2SI0VmTicczq0bzcE9y4ukcXE9CjZd7a/WUjFsq06QjJrmWX3j3eho5KJbJNkyuOKsS8Pb7RB7YOPOnsXqHmWxFJHGdQcdXXw+dda2iHK1ZlsjKytFBPIB1B9B8gXaNzMeM/a4ztonwj7A5iUjjNIf8AaO+pfJ3U+mftI2seiPsY3ZOvMuPpc4rzO4e427E6+B5NB9NT9K8Tme4wsT7p7ugD0ALxOSXuMcMLnOdzK5zL3EPiPogICAgIPTWOdyC+xEy+TMQ27XFXlx/NxOcOkgcB6SeC7Y9te/aHDJuaU7ymrDaU8hHiuDW9Onj/AKXL2Kww8bae6vzclWOy5YXbdrZt1QR0eR3pn8/Ur/a7CuP8sfWoN1v7ZPzT9SfhgZGOt3IuKsq0iFba8yyL28CAgICAgICAgICAgICAgICAgICAgINLI4u2vYXRysDgVHz7euSNJSMG4tjnWHN9wbOurJ7pbdpfFzoOYWT3vF2xzrXs1my5SuSNLdJVlzXNJDhQjmCqiY0W8Tq+I+iAgICAgICAgICAgICAgIM9tZXVy8NhjLyeoLpjxWvOkQ55MtaRrMpWLad+W6pnxwD8t4BU2vG38ZiPrQrclT92Jn6m5DtK2qNdyZD92KNz/loAu9eNr421+iJlHvyVvCun0zEJK22jb17lnPL2v0xj6SpePjK+FbT9yJk5O3jasfelLbacopS2ggA6XEyO+Xh8imY+Nn+Gsfeh5OSj+K0/cmLbAQsoZXayOQ6B6ByCn49lEd+qBk3sz26JGK1gj91or1niVKrjrHZFtktPdlXt4EBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQeJYY5GlrhUFebVie71W0x2VfObIs7zVJCPDl51Cp93xNMnWOkrnZ8tfH0nrCiZPbWSsHHXGXMH22hZvcbDJinrHRpNvv8eWOk9USQQaHmoSaICAgICAgICAgICAg9xxSSODWAknoC9VrM9nm1ojusuE2TeXpD5qxx/KrbacTfJ1t0hUbvlqY+lesrvYbPxNqwAtc93TVxp7KrQ4eMxUj0s9m5TLefQkosTjYvct2DtoFLrtsde0QiW3OS3eZbLY428GtA9AXaKxDjNpl6X18EBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQYpraGVpa9oIPWvFscW7vdck17K5lti467a4wtEUp5OA4V9CqtzxGPJHTpK123MZMc9esKnf7BylvUxkSgdlFSZuGy07dV3h5nFfv0QVzir+2JEsLm06aKtybfJTvCyx7jHftLVII5ii46O+r4gICAgICAgyRwySO0saXE9AXqtJns82vEd0/idm5C8Ic9pjjPSVZ7bi8mTv0hWbnlMePt1leMPs+wsmhzmh8nWVotrxmPH37s5uuUyZO3ZYGMYxulooArOIiOysmZnu+r6+CAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgEA80GCaxtZhSSMH1LnbDW3eHSua1e0oq72fh7itYgCekKFk4zDfwTcfKZqeKHufLizdUxSFvyqBk4Kk9pT8fO3jvCLn8t7tv83MD6aqHfgrx2lMpztJ7w0pNg5hvuhrvzh9Kj24bNCRXmcMsDtkZxv9ED6HD61znic/odI5bBPi+DZecP9D8oXz/is/off+Vwelmi2JmXHvNa30kLpXh80uduYwwlLLy7lLgbiQU6QFMxcJP70oeXnI/dhacZtXG2QBEYLh0nirnb8djx+Cm3HI5MnimGRsYKNAAU+KxHZAm0z3el9fBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQECg6kHzS3qC+aQ+6yaGdQTSDWTS3qCaGr6vr4ICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgg917uxm27IT3dZJ5ai2tmU1yEc/Q0dJULfb6m2rrbvPaPS82tEKDtTzP3DnN4WdrKYosdK+RjoImDiQx1Kvdqd3SBxBFSqTZ8tmy7itbaRWfD6ujnW8zLrS1DsICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIMd1cRW1tLcymkULHSSHqawVPyBeb3itZtPaB+aN27jv87lX3MzyJblxbAytRBCKmjf4IP8AGK/PtxubZ8k5L9vR7IRZnWdU55XWPi7yxzI20itw+QgcmtZG4N+WgUjh6TfdVn6Z+59p3d/W7SRAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEGG8vLSzt33N3MyC3jFXyyENaPWV4yZK0rNrTpEPePHa9vLWNZlULvzd2hA8tjNxcgcNUUVAfR4jo1T5P1Btq9vNb6I/HRb04Hc27+Wv0z+GrNYeauz7uVsbp5LVzuANxHpbXtc0vA9ZXrFzu1vOms1+mHjLwm5pGukW+iVuY9kjGvY4PY8BzXNNQQeIIIVxExMawqZiYnSX1fXwQEBAQEBAQEBAQEBAQEFb8x7mS32VlHxmjnMZGT+TLI1jvkcq7lrzXa3mPR7Z0eb9n5wtXeNeXM/2YyLeP8AM4vPrcaepYa8aViPrRnbPJ3bUlpZTZu4aWyXg8K1aRQ+EDVzvz3Dh6O1ajgNnNKzlt+92+h2x18XR1onUQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBByjzDurvP7zstsQSaYInRtfTl4kg1PeevRGeHrWS5e9tzuq7eJ6Rp9s+P1Q1XFUrt9rbPMdZ1+yPD65XzF7L2xjrZsEOPhkLQA6aaNskjz1uc4Hn2cFf4ON2+KukUj6ZjWVFm5HPktrNp+qdIam4/L/b2XsZI4rSK0vNJ8C5hYIyHDlqDQA5vXVcd5xODNSYisVt4THR22nKZsN4mbTavjE9Vd8n81dPivcFdEl1kfEga48WNLtMjPQ11D61Xfp7c2mLYbfu9vfCw5/b1ia5a/vd/ctO7d42O2o7aS6glmFyXtYItPDQATXUR95Wu/5Gm1iJtEz5vQq9jx991MxWYjy+lF7h80sHiLj4WOOS9uWgGVsZaGRkiulzzXvddBwUTd85hw28sRNrexK2nC5c0eaZitfatlhdC7sbe6DdAuImShhNaa2h1K+tW+K/npFvTGqpy08l5r6J0Vbc/mLBgso7HDHy3czWMkLmOAbR/qcVVb7mI2+TyeSbTotNlxM58fn80VhH2XnHhnztiv7K4sg7nJwkDe1w7rqegFR8f6ixTOl62r96Rk/T+WI1paLfcvlvcQXMEdxA8SQStD45Gmoc1wqCFf0vFqxas6xKivSazMT0mEDn97Y/CZezxlxBK+W90FkjNOhut+jvVIPNV+75OmDLXHaJ1t+OidteOvnx2yVmNK/hqhMt5w4G0uHw2dvLfaCQZmkRxkg/ZJqSO2ig7j9RYaW0rE39faE7BwGa9dbTFfvlu7a8zsHm7xli6OSyu5OETZSCx7vuteOnqqOK77Lm8Oe3k0mtp9LjvOGy4K+fWLVj0LgrlTqluXzKwWDunWWl95es4SRQ00sPU55PPsFVUb3msO3t5OtrepbbPh82evm6Vr60dYeb+JknZHkbGewY80EzvxGDtdQNd7AVGxfqHHM6Xranr7pGXgckRrS0X9XZOWG9sde7lmwEUMnjwhzvHq0xODQDVpBrxqp2Lk6ZNxOGInWPHwQsvHXpgjNMxpPh4p27u7aztpLq6kbDbwtLpJHGgaAp+TJWlZtadIhBx47XtFaxrMqLcecGLM7osdjrm+oaB4ozV2gUe72hUN/1Dj10x0tdeU4DJpre1atLKb/w+5sJfYV0Mtjf3EdLdktCx8rSHtYHDkXFtBUBcc3LYt1itimJpeY6a+n0I+84TNjpNqzF49Tkm0hjIIrWbLAmJwNx8NqEZn1vcffdybXmRXq9FPGOPmea8TNY8PSz+nV0qTzey7dLbW0tIIGijIzqdQAcACHMHyK1nnMv7taxH1vfzJTe2fNhmSvoLC6sqzTuEbJbV2tuo9bTyHbqU3a8xN7RS9es+j8HqMjoSvXQQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEHJ8SBL5zXDnA1ZJcaa9kJaPVTksjt+vKz9Nv8AtavP04yPoj2usLXMoIOT7EHheaGajZwYHXzKdguBT5lkeL6chkiP8f8A3NXyfXYY5n/B/wBrc87f+UxP6Sf5mLt+pvy0+mfc4/pz81/oj3rdtHbGPw2Mg8ONr72VgkurtwrI+R4q7vHjp48Arnj9lTBjjSPinvPjMqjf7y+fJOs/DHaPDRPKegtC4z+AtZTFc5K1glHvMknjY6vaHOBUe+7w0nS16xPrmEim1zXjWtLTH0SqXmNktqZLa90GX1nc3sOh9qI54nyh2ttQ0Al3FtahVHMZttl29tLUtaO3WNe614nDuMW4jWtorPfpOnZIeVskj9lWWtxdodM1tegCV1ApHB2mdpXX1+1H5qIjdW09XsVLzZtfi924i11aPiIo4tfOmuZza+qqqOfp59zjr6YiPvW3BX8m3vb0TM/c6ZicPjcTZstLCBsMTABwA1OI+053NxPWVp9vt8eGsVpGkM1n3F81vNedZcz81rK3s9y4i+tWCG4n70j2ClXxSNLXn8rvc1mOex1pnx3r0mfdLS8Hkm+C9LdYj3w6bmr02GHvr0e9bW8sra05sYXAce0LT7nL8vFa/wDDWZ+5mtvj+ZkrX+KYhzrydxNtcm/zVy0TXbZRFE941FhI1veCftO1Dis5+ndvW3my26210/Fof1BntXy4q9K6a/g6Ve2NnfWz7a8hZPbyCj43ioP+ftWmy4q5K+W0axLN48tsdvNWdJco2NjzjvM26sNRe22bcRxucakxtoGV/MoslxeH5W/tT+HzR9Xh9zV8ll+ZsYv/ABaT9filvOnJSxY7H49jiGXMj5ZQDzEQaGg9lX19SmfqTNMUpSP3pmfs/vRP07hib2vP7safb/ct2Dt8FgcTBZxzW8Hhxt8Z5expe8DvPca8SSrfa0w7fHFYmsaR17faqdzfLuMk2mLT16d1K825tuXmMgurW7tpcnDM1pEMsbpDE5rq6g06qAgU6vWqTn7YL44tW1ZvE+ExroueCrmpkmtq2ikx4xOmqz2OBw25NqYp2UtWSuNpFpeBpc0lgDtJHQTxpyVtgwY9xt6TeOvlhQ73FWua9Y7RaUG/yVwXiVhunxsrUt8KMmnYQG/Mo1uDrM/nsh/LWjAbMwWDPiWkJfc0obmU6n06acg31BT9rx+LB1rHX0y9RWITimvQgICAgICAgICAgICAgICAgICAgICAgICAgIOS3bv2P5xMnnOiC5kaWvcaAtuIfDqewSH5FkMk/I5SJntaf+6NPa1mOPncbpHeseydfY60teybzLJHFG+WRwZGwFz3HgA0CpJXy1oiNZ7PsRMzpHdynyrY6/3fmMvQmItlIca+9cSh4qevS1yyXBR8zdZMvh1/90tTzc/L22PH49Puhuedv/KYn9JP8zF2/U35afTPucv05+a/0R73R7T/AJSH9Gz5gtLj/LH0M7k/NP0qf5r5y8xm3o4rSQxS3svhPkaaOEYaXOAI5V4D0Km57dWxYIis6TadPqW/B7auXNM2jWKxr9bT255WbalwtpcX7JLm6uYmTSO8RzGtMjQ7S0NpwFelcdnwWCcVbX1ta0a9/S67vm88ZbVppFYnTs1t67A2lidtXl9bQuiuYwwQOdM81c54FAHGh4VXPkuJ22Hb2vWNLR26z6XXjuU3GbPWlp1rPfpCc8qv3LtP0k/6xyncF/SV+mfag83/AFVvoj2K35lfv3t//Yf/AHBVZzX9Zi+r/uWXD/0mX6/+11Jatl3LfOL+9cH/ALT+XGsp+ov9zH9fthqP0/8A7eT6vev26rd1xtnKwtBL32k+gDmXCMkD2rQb+nm294j+GfYodjby56T/AIo9qneSt0x2GyFrq78VyJS3pAkjDQfX4apv01eJxWr6La/bH9i3/UVJ+bW3prp9k/2uirSM85Tta6huvNrIzwnVG74kNcOR00bUdhoslsckX5K8x2+Jqt7SacdWJ7/C9+d1vJqxNwBWOk8bj1HuEe3j7F6/U1J/07eHX3PP6ctHx1+j3pu08qNmSwMnZ8RLHM1r43GX7LhUEaQOdVOx8DtZjWPNMT60K/ObqJ0nyxMepux+VuyGVrYOf1ap5uHseF2jg9pH7v3z+LjPNbqf3vuj8FmtLW3tLaK1tmCOCFoZFGOTWtFAOKtMeOtKxWsaRCtve17Ta3WZZV7eBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBBVd97Hi3JbRyQyCDJWwIhldXS5p46H040rxB6FU8pxkbqsTE6Xr2/CVpxnJTtrTExrSe/4qzBc+cOJiFqbVt9HHwjlfolNP4TXtcfzuKq6X5TDHl8vnj6p9/tWdqcbmnzeby/bHu9jxeWfmxuSH4O7iZj7GThNxbE1w/KAL5COzkvmTHyW6jyWiKVnv4fjL7jycftp81Z89o+v8IXnaW17TbmKbZQu8WVx13E5FDI88OXGgA4AK92GxrtcfkjrPjPpUm+3ttzk809I8IQHmltrN5y3x7MXbfEOgfKZRrjZQODae+5teXQq/nNll3FaRjjXTXxj3p/C7zFgm05J0109PuXW2Y5lvExwo5rGgjtAV5SNKxClvOszKC3ztc7jwhtIniO6ieJrZ7q6dYBBa6nQ4EqBymx/mcXljpaOsJ3G73+Wy+afyz0lUcdfebOGtIsYzFRXcduBHDLINfcHBo1xysFAOVVT4cvJYKxjikWiO3j7LLbNi47Nacnnmsz+3jD3kdob53FY3Fznp2slijc6wxMDmhpmp3S8g6PWXE9oXrNx+73NJtmnrEfDSPT7PvfMO/wBptrxXDHSZ+K0+j2/ctPl/ichidsW9lkIvBumPlL49TX0DnkjiwuHI9ateJ298O3il40t19qr5XPTNnm9J1r09iF3vtjOZPdmHv7G28a0tfC8eTxI26dMxeeDnNce71BQeT2ObLucd6RrWumvWPSm8bvcWLb5KXnS1tdOk+hfVoFCoXmTtjOZnIYqXG23jx22vx3eJGzTVzCPfc2vI8ln+Z2ObPek441ivfrHq9K+4fe4sNLxedJt26T6/QvpAIoeIPMLQKFzK82BujBZiTJbRnaYZK/2VzmhwaTXwyH9x7a8qmo+VZfJxO42+Wcm2npPh7uvSWlx8pg3GKKbmOsePv6dWwLXzdyo+EvHw4u2k7s07DFr0Hnp8Nz3V9BHpXTycnm+G2mOs956e7X3Ofn47F8Vdb2jtHX36IjY9hbWfmbd2dnV1tZxzRBxNT3A1jie0uUPjMVab+1aflrEx9nT2pnJZbX2NbW72mJ97oe7dtW+4sO+wld4cgIkt5qV0SNrQ06QQSCtHv9lXc4ppPSfCfWz2x3lttk88dY8Y9Si4+HzY23ELG3tmZCyiNIalsrQOptHRyAdhVDiryW1jyViL1jt4++JXmW3H7mfPaZpae/h+MNk3fnFk6xstosax3vSgMZpB/hulf7AuvzOUy9IiKfZ/bLn8vjcXWZm/2/2Q6UtMzYgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgqe7d2Z/C5COKxwsmRtHwte6djZKNkLnAsJa145AH1qo5Df5sF4imOb1079e612Gxw56TN8kUtr26dkBdb+3zkGOtsXt2a1leKCZ7JJCyo5guZGwHtcq/Jyu8yR5ceKaz6es+6IT6cXtMc+bJli0fVHvmUz5ebKucFFPfZJwflbzhIAdXhsrqLdX2nOdxcpvEcbbbxN8n+5b7v28UPluRrnmKU/JX71yV0pxAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEGtk8jb42wnvrnV4Fu3XJoGoho5mnYuWfNXFSb27Q64cVsl4pXvLWwG4cbnbJ15j3ufC2QxOL26SHNAJFD2OC57Td49xTzU7a6Om62t9vby376apJSUYQEEJZ7wwt5nZsJbve++gLxKNB0Ax8Hd708FBx8jivmnFXXzRr9ybk2GWmKMs6eWfem1OQhAQQ+8Mpd4vbd9kLQgXEDWujLhqFS9reI9BULkM9sOC1694/FM2GGuXPWlu0/gwbFzV9mdtwZC9LTcSPka4sbpFGvLRw9AXji9zfPgi9+86+175Pb0w55pTtGifVggCAgx3VxHbW01zLXwoGOkfQVOlgLjQegLze8UrNp7RGr1Sk2tFY7yqP+LWzv62b/cuVP8A8/tfTP2Lb/gtz6I+0/xa2d/Wzf7lyf8AP7X0z9h/wW59EfatWPvre/sYL23JMFwxskRcKHS4VFQrbDlrkpF69rKvLinHea27w2F0cxAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEFHwG7sxe79yOEndGbG28fwgGUd+G9obV3oKotpyGXJvL4p08tdfuld7rYYqbSmWNfNbT714V6pBAQEBAQa+Rs2XuPubN/uXMT4nV6ntLfpXPNjjJSaz+9Ew6Ysk0vFo8J1c38mLt8U2WxUvdewslDOotJjk/wDCsz+m8kxN8c/T7p9zR/qHHExTJH0e+PevO7sqcVtrIXzHaZY4i2F3VI/uMPqc4K+5DP8AJwXvHeI6fTPSFHsMHzc9aeEz93eXPPLvdGfO5oLLL3c00N/A427ZnlwrTWx4ryqGOCznD77N/MRTLaZi9emv2+5oeW2WH5E2x1iJpPXT7HV55o4IJJ5DSOJrnvPU1oqVrb2isTM9oZWtZtMRHeXMfKC3ku8pmM3KO/IQwHn3pXmR/H81qy/6epN8mTLPj7+stLz9opjx4o/bTpC/57cWKwVn8VkZfDY46Y2NGp73dTW9PzLQbreY9vXzXn+1Q7XaZM9vLSFNPnFavJfb4e5lt2+9LqAoK04gBw+VU3/kVZ61x2mFx/4/aOk5KxKxbX31hNxF0VqXw3bBqdazAB2n7zSCQ4Kx2PKYtz0r0t6JV294zLtututfTD55i/uXlP0bP1jU5j+kv9Hvh94n+qp9PulVdp70xG3Nj2Autct1M+cw2sQBeR4rhqNSA1tf8yqdhyWLa7Svm62nXSI+lab7jsm53VvL0rGnWfobLPOWwbKBd4q5giJ4PDmuNOvSQz510j9R0ifipaI/b6HOf09eY+G9ZlecVlrDK2Md9YSia3k5OHAgjm1wPEEdSvsG4pmpF6TrEqTPgvivNLxpMNtdnFoZ/wDuHJf9LP8Aq3KPu/8AZv8A5Z9jvtf96n+aPa5x5S4HDZLG38mQsobp8czWsdKwOIBbWgqs3wG1xZcdpvWLaT4tFzu5y4r1ilpr08F8/wDRe0v/AMRa/wC6b9Sv/wDjdt/9dfsUf/I7j+O32pa3t4LaCO3t42xQxNDY42ijWtHIAKXSkViK1jSIRL3m0zMzrMqpYeZ2AuYb+eZstrFYaQ8yaSZHPLg1sYaSS46VU4ubw2i0zrWKenx+ha5eGzVmsRpab/d9LLtPzAs9yZG4tLa0kgbBH4okkc2rhqDaaW1pz6162HLV3V5rWsxpGvV533F221Ita0TrOiV3NuCHAYp2QmhfOxr2M8NhAcS80HNS97u42+PzzGqJs9rO4yeSJ0U7/GW3YazYe4ji+/rFfYWtHyqm/wDI6x3x2iFx/wCP2ntkrqt22914fcNs6bHyHXHTxoJBpkj1cqjiONOYKuNnv8W5rrSe3ePGFTu9jl29tLx37T4S2s5l4cPirjJTsdJFbAOcxlNRq4N4VIHSum63EYMc5J6xVy22Cc2SKR0mVWuPNrb0ONt7vwpnz3GostBp1ta1xbqea6QCRw6VVX5/BXHFtJ1t4fitKcFmm811jSPFrYzzkwlzcshvLWWyY80E+oSMbXpdQNIHoBXPB+osVraWrNfX3dM36fy1rrW0W9XZIbj8zsBhrg2jGvvrpoBe2EjQ2oqAXk8/QCpG85vDgt5Y+O3q7faj7Ths2avmn4a+v8GphPN3B5C7jtbqCSwdKQ1kr3B8eomgDnChb6aUXLbfqDDktFbRNNfsddzwOXHWbVmLafavavlGrm59+YLbzxBcufPeOGoW0IBcAeRcSQG/Oq3fcrh206W629ELHZcZl3Ea16V9Mq8POK1aQ+fD3Mds492XUDX0Aho/0lXf+RVjrOO0VWH/AI/aekZKzK54LP4vOWIvMdL4kddL2kUex33XN6Crra7vHuKeak6wptztcmC/lvHVH2W9LC63PcbdMEsV3Br/ABH6dD9FD3aGvFpryUfFyVL7icGkxaEjJx164IzaxNZWFWKvU/FeZ2EyWdZiIYZmPlkfHHcP0eG4sBI5OJ72ngqbBzeLLmjFET1nTXwW+fhsuLF8yZjpHbxXBXKoV/Hbzsb/AHLdYG3glM1pr8ac6fDHhkNdyNfeNOSrsPJUyZ7YaxOtddZ8Oiwy8dfHgjNMxpbw8erez24sVgrP4vIzeGwnTGxo1Pe7qa3pUjdbzHt6+a86e9w2u0yZ7eWkKY7zls3urbYi5lirQvLmg+xoePlVJP6jrP5cdphcR+n7R+a9YlN7a8x8BnbltozXa3jq6IZgKPp0McCQT2GhU7Zczh3FvLGtbeifchbziM2CvmnS1fTCyXt0y0s57p4LmW8b5XNHMhjS4gV9Cs8l4pWbT4RqrsdJvaKx4zoqlv5o7ekw0uUlZLAxkphjt3BplleGhx0AOpQB3EkqppzmCcU5J1jrpp4ytb8LmjLGONJ6a6+EN/Z284NzMu3w2z7dtq5je+4OLtYJ6BwppXfjuRjdxaYjy+Vw5Dj52s1iZ18yn7T/AP6zmf8A6r9Y1U2w/wD6WT/1e2Fvvv8A+dj/APT7HQs7nLDCY2W/vn6Yo+DWj3nuPJjR0krR7rdUwY5vftH3s/tttfPeKU7tbbG5G7gs3XsNnNbW1dMUk2keIRz0hpPAda5bHefzNfPFZrX1+Lpvdn/L28s2i1vV4JlTUMQEBAQcpxg/YvnBcW9NEN+6QcOVLhgmH/EFFksH+hyc18L6/wDu6+1qs3+vxsT410+7p7Ep5v3kj7LG4WDjPf3Adp6wzutB9L5B7FL/AFDkmaUxR3vb9vvlF4DHEWvlntSv7fdCO8xMeMBfbby1q3uWAjtnUHMW5DmA/wAJupRuYxfy18OSv7mkf9Pb3pHE5f5imXHb97Wft/aFv37lI7bZeQuY31FxCIoiD73jkM4fmuJVxyueKbW1o8Y0+1UcXhm26rWfCdfsaflVjvhNoQSEUfeSSTu66V0N/wBFgK48Fh8m1if4pmfd7nbm8vn3Mx/DER7/AHqvujw855p2uKu3VsbXQxzC6jSBH48g/O90qq32m45GuO35a/h5p/BabLXBx9slfzW/Hyw6UMjhLONsQura3iZ3WR+JGxradAFQAtP87FSNPNWI+mGb+TlvOulpn6Jcv3Fc4m08x8TkMJPDI2d8PxPwz2ObrdIY5ASwkAvYePtWV3l8dN9jvhmJ1mNdPp0nt6YabaUyX2V6ZYmNNdNfo1jv6JXvzF/cvKfo2frGq/5j+kv9HvhR8T/VU+n3Sr/lFgbRuG/bE0Ylu5ZHxwSO4+FEw0oyvu1fqJoq79P7WsYvmzGtpnp6o/vT+e3Vpy/LidKxHX1z/cvOUxdjlLGWyvYhLBK0tII4gkU1NPQ4dBV7nwUy0ml41iVJhzXxXi1Z0mHN/J24nt8ll8Q92qOOkgHQHxvMbiP4VR7Fmv07ea5MmKe34dGj5+kWpTJ+3Xq6ktWy7Qz/APcOS/6Wf9W5R93/ALN/8s+x32v+9T/NHtcc2NvHJYG0uYbTFuv2zSB7ntLhpIbSnda5Yzi+RybesxWnn1n9vBsOS4+m4tE2v5NIWb/FfcH/AOuSe2T/AOWrP/ns/wD9U/f+Ct/4PD/9sfd+LpFrK6a2imc3Q6RjXlvUXAGi01LeasT6WcvXy2mPQ435b7ds8xuG+kvh4trZHxPhne5JIXuDC8ciGjVwWL4bZ1z57TfrWvXT0z4Nhy+7thw1inS1vH1OzRxxxsEcbQxjeDWNAAA7AFtYiIjSGOmZmdZYby/x9mwSXtzFbM6HzPbGPUXELxky0pGt5iv0zo948V7zpWJn6GjJubacsbo5MtYPjeCHsdcwkEHmCC5R53u2mNJvTT/NDvGz3ETrFL/9MueeX3wsHmPlYMe9psHMuBD4bg5hYJWlukjgQOgrO8T5a768Un4Pi09GmsNByvmtsqTf8/w+yV28xf3Lyn6Nn6xqvOY/pL/R74UvE/1VPp90oTyiwlhFt8ZTw2vvbmR48VwBcxjHaQ1p6OVSoP6f21Iw/M0+K0z9ybz25vOb5evw1h784MfaS7abeujHxNvNGGS072l9QW16ulff1DhrODz6fFEw+cBltGfy6/DMS3fLPBY+y21aXscbXXl60yzXBA18SaNB5hoC78LtaY8FbxHxW6zLjzG5vfPasz8NekQhvOjG2n7Js8iI2i6bcCAyAULmPje6hPTQx8FC/UmGvy630+LzafdP4Jf6ezW+Zamvw+XX74/FdduTuk23i7iZ1Xvs7eSR56zE0kq82d9cFLT/AAV9im3ddM94j+Kfa5r5cR2ea3Plc7lND3xESRCYto18znEEA8O41lB1LMcNFc+4vmyeHp9f4aNHy82wYKYsfj6PV+Orpd3mNu+E+K8vrTwnikjJZotJaeHEOPJafJuMGmlrV09cwzePb5tda1tr6olzry3lgtN+ZjH2MolxsjZjCWODmERyjwyCCQ6jXEVWb4a0U3mSlJ1pOun1T0aLl6zfaUveNLxpr9cdWfeg/YvmRh80O7FdGNszuXunwZP+E9q6clHyN/jy+FtNfZP3OfHT8/Y5MXjXX8Y+9dt45X9l7ZyF4DpkbEWQn/WSdxh9RdVXnI5/k7e9vHTp9M9FLx+D5uetfDX7o6uSTYKbDbWwW5oW0uvinSyHsJDoa9lIj/GWQttZwbfFuI/N5tf/AI+z72sruYzbjJgn8vl0/H2/c7ScnbDFHJg1thB8SD/q9GuvsW2+dX5fzP3fLr9WmrGfJt8z5f72un36KH5PWckzcrnJxWW7m8MO7R+JJ7S8exUH6exzbz5p72nT3z7V9z+SK+TFHasa+6PYjd0tbn/NO0xFwS+ztzHGYweBaI/Hk5fe90qNvo/mORrit+Wukfd5pSNlP8vsLZI/NOvt8sOrQwQwRMhgjbFFGNLI2ANa0DoAHBaytYrGkRpDLWtNp1mdZcw84MTb2cmPzdm0W92ZTHLJGA0uc0a43mn2m6Tx+pZb9Q7etJplr0tr/bDTcBnm8WxW610/slebq8N7s2W9PA3OOdNQf6yAu+lX18nzNrN/4sev21UdMfk3MV/hvp9lnPvKPbVhfi5yl8wXAtZBHbQPGpjXloLnlp4E00gf+xZ39P7KmTXJfr5Z0iPe0HPby+PTHTp5o6z7nWgA0AAUA4ADkAteybkmDyVhjfM/N3V9Oy3gZ8VV8jgKnW06R95xpwA4lZDa5qYuQyWvMVj4va1m5w3y7DHWkaz8PsZ7W1yHmJnje3Qfb7asXlsMfIyEUq3+E77R6BwXulL8nm89umCnb1/2z4+h4venHYfLXrmt39X7eHpdRgght4WQQMbHDG0NjjaKNa0cAAAtVWsViIiNIhmLWm06z1mXtenkQEBAQcs81GOxm6MLnYxSmnUR0ut5A/j6WvospzsfK3GPNH7eWdfe1HCT83BkxT+3mj+xtSuGe82oGsOu0w8IfXoqwawR/tJW+xdbT/MclH8OOP2++XKsfy/HT/Fkn9vuhYPMzGfH7PvKCslppuo+zw/fP8QuVjzWD5m1t6a/F9n9mqBw+b5e5r6LdPt/t0c/3DuB1/5e7fsGOLrh8ropGg1J+FHhtB9IkaVnt3u/mbLFSO+un/T098L/AGm1+XvMt/DTX/q6+6XX8VYtsMZaWTaabaGOIU/IaG1+RbHBi+XjrT+GIhkc+T5mS1v4pmXJc9hbO982H2GSL22l69h1MIa46rcaKE1/pBpWQ3W2rk5KaZPy2/8Aj0+9q9ruLU4/z0/NX/5fguMXlJs5lNUU8lOeqYiv8XSrqvAbWPCZ+tUTzu5nxiPqb1l5cbNs54riHH/jwuD45HSyuo5pqDpL9PA9i74+H2tJi0V6x65/FwycvubxMTbpPqj8HrzF/cvKfo2frGr7zH9Jf6PfD5xP9VT6fdLR8p7yOfZtvC1wL7WSaN7ekFzzIK+p64cDki21iP4ZmPv197vzmOa7mZ/iiPZp7lxJABJNAOJJVyp3KvKo/Fbuzd+zjG5slHCtPxZtY9ulZPgvj3OS8duv32anm/h22On0fdDqq1jLNDP/ANw5L/pZ/wBW5R93/s3/AMs+x32v+9T/ADR7VH8lP7qyX6dn8hUX6a/27/5vcu/1F/uU+h0daVnRBy3yd/vXOf7P+XIsp+nf9zJ9XtlqP1B/t4/r9zp9xM2GCSZwq2JjnkdjRVam9vLEz6GZpXzTEelyHZeCj3tlMjlc9LJKInMDYWOLRWTUQ2vMMYG8AFj+N2sb/JfJmmZ08Pp90NbyO5nY46Y8MRGvu98rn/hVsv8A8pJ/v5P/AHldf8FtP4Z+2VN/ze6/ij7IVLYFtZ2vmVkbayobSFlzHDR2oaWyNA73GqqOJpWm/vWn5Y82n2wtuUva+xpa35p8uv2SvHmL+5eU/Rs/WNV7zH9Jf6PfCk4n+qp9PulqeVX7l2n6Sf8AWOXHgv6Sv0z7XXm/6q30R7GPza/c6X9ND/KXnn/6Wfph64L+pj6JSmw/3PxX6BvzlSuK/paf5Ubk/wCpv9KA85/3Xtf+uj/Uyqv/AFJ/T1/zx7LJ/wCnv6if8k+2qzbaj8TaOKjrTXj7dteqsLQrTZRrtqR/gr/2qzeTpubz/jt7XJ/LvaOIzt3kbTKmVlxaiMxsjcGnm5slah3I6VkeI4/FuLXrk11rp2+vX3NXy2/yYK1tj00t+0L+zyn2Y3nbyv8A4Uz/AKCFoY4Hax4T9sqCec3U+MfZCXw2zdt4W5dc42yEE7mlhk8SR50kgkDW51OSmbbjsGC3mx10n6Zn2yibjkM+evlvbWPoj3K95wYz4nbUd40VfYzNcT/q5O47/SLVXfqHB59vFv4J+6en4LDgM3lzzX+KPvjr+KF3tmpc5t/bONt3arnLGKSYDj32gR0PZ4jnexQuT3M7jDhx1/Nk0/D2+xN47bxgzZrz+XHr+Ps9q6bpwEV1s26xUDeEFu34VvTqtwHMA9Oiiut9tIvtbY48K9P/AE9lNst1NNzGSfG3X6+6iw7nr5RTQl39pjk/Zw6KtedY/wCFUepUNd7/APrJj96J8nv9i8ts/wD9jE+Gnm93tXzYmL/Zu1MdbubpkfEJpR06pe+Qe0aqK/4vB8rbUr46a/b1UXJ5vm7i0+Gun2dFFnJsfOmOSXg2aRvhuPAHxrbwxT851FQ2/wBPlYmfGfbXReV+Pi5iPCPZbV1ha5lHOfOuVgw+OhJ777hz2jsYwg/ygs3+pbR8qkf4vc0X6drPzLT/AIfes0UL4dgMhkFHxYkMeO1ttQqzrWa7LSfDF/8AirZtFt5rHjk//JXPJb+4b7/qv/htVb+m/wDZt/m90LD9Rf71f8vvl0NaNn3GrTb9jnvMfOWF7q8Mm6exzSQWvDwGu7aV5FYvHtKbjfZKX7fE2OTdX2+yx3r3+FJ7Ozt5tPNP2pnCG2rnk2dx9lpeatIP9XJ8jufSpXHbq2zyzts35fCf28J+6UbkNtXd4o3GL83jH7eMffDqS1TLiAgICAgqfmVtu+zuCjisIvGvYJ2yRs1NbVpBa4VeWt6QefQqjmtnfcYYika2idVtw+7pgyzN50rMNDyx2nl8Mchd5iIxXlyWMjDpGSEsbVznEsc73iRzPQo/CbDLg81ssaWt69fY78zvseby1xzrWPVou9xBHcW8tvKNUUzHRvHW1woR7Cr29YtWaz2lSUtNZiY7w4/t/wAtt0QbhsnX1pTGW1yJXSGaItIYQa6A8u7+ho5LG7ThtxXPXz1+Ctte8ezXxa/dcvt7YbeS3x2rp2n8PB2RbRjlN3/sWXPGDIY6UQZW1GlpcS0SNB1NGocWuafdKpeW4udxpek6ZK/euOL5ONvrS8a0t9yFhzHnDYxi3lxkd25o0iZ7Wvceo6opGgn0qDXccnjjyzSLev8AulNtt+NvOsXmv7euErtw+Zl3mra4zTY7TFx+IZbdvhgvJY5reDS93BxB4kKXs/5++WLZdK4416dPR9c90Xd/yNMU1xa2vOnXr6fqTu9MdeZLbF/ZWUfi3UzGiKPU1tSHtJ4uIHIdan8lhtl29qUjW0/ig8dmriz1vadKx+Cj4nZ299vY61v8Np/aEjHNyeMkexzHESOMbmmug9ynJ1eo8VR4OO3e2pW+L88/mrOnp6ert613n5Da7m80y/k/dt9XX19/U93955t5q3fjzjo7GKYaJZYx4fdPMF75Hmn8HivuXJyWevk8kUifq9sz9zzix8dgnz+ebTH1+yIW3ZG0o9t4owOeJbydwfdTNHAkCjWtr9lqt+M4+Nrj072nvKp5LfzucmvasdliVkr2pmIJbjE3tvC3VLNbyxxtqBVzmEAVNBzK47is2x2rHeaz7HXb2iuSsz2iY9qq+V+3czhMfexZS3+Hkmla+Nutj6tDaV7jnKp4PZ5dvS0ZI0mZ9XuWvNbvFnvWcc6xEev3rqrxSiCheW22M5hshlZclbeBHc6PAd4kb9VHPJ9xzqcxzWf4bY5sF7zkjSLdusev0L7mN7izUpFJ1mvfpPq9K+Oa1zS1wq1woR2FX8xqoYnRyuDbG+tn5S6m2/Cy/wAfcGgY4h1WgksD2amO1NqRVpWUrst5sslpwR56T+0ax0n7GptvNpvMcRmnyXj9p07pEO82M8z4WaKDCWr+7NcMGl+np0gvkfX0afSpGvJbiPLMRir4z4+2Z9iPpx+3+KJnLb0ftER7TZ+xsngd6XNwIi7EiB0cF058ZLy7QeLQ7UOIPQvvHcXk2+6m2n+np0nWPV9Zv+Sx7jaxGv8Aqa9Y6+tat6Y68yW2L+yso/FupmNEUeprakPaTxcQOQ61a8lhtl29qUjW0/iq+OzVxZ63tOlY/BreX+JyGJ2xb2WQi8G6Y+Uvj1NfQOeSOLC4cj1rnxO3vh28UvGluvtdOVz0zZ5vSda9PY8eYmHyWX21JZY6Hx7l0sbhHqazg01PF5aPlXnl9vkzYJpSNbax+3V94ncUw54tedI0lIbSsbqw23j7O7Z4dzBEGSx1DqOBPCrSQfUVI2GK2PBSto0tEOG+y1yZ7Wr1iZQ/mbgcrmsDBa4yD4idl0yVzNbGUYI5Gk1e5o5uCh83tcmfDFccaz5tfD0T6Uvht1jwZptknSPLp98ehP7ftZ7TA421uG6J7e1gimZUHS9kbWuFQSDQjoVhtKTTDStu8ViJ+xA3V4vmvaO02mfvUfcmw9wWeefn9qShs8ri+W21NY4Pd7+nX3HNceJa5UW84rNTN87bT1nw/bpovNnyeG+H5O4jpHj+3XV4G4fN7T4X7Hi1/wBb4fHl1+Lo+RfP5vk+3y4+z/8A1o+/ynG9/mT9v9mqy7Ni3mDeT7mezVL4QtYGFh8MN16/5vu8at6SrPjq7r4p3E99NI6dO+vZW8hba/DGDw11nr17ad0vnscMlhb2wIqbmF7GdjiO6fU6hUvdYfm4rU/iiUTa5vlZa39Euc7F2Lua13DZ3eatvCs8dFJ8NqlieA5xcQ0BjnH3pXPWb4vi9xTPW2WNK0idOsT7J9cy0XJ8ngvhtXFOtrzGvSff9EQ6otYyzjlx5a7nOcltIrf/ALBLetlL/FiAEQcQHaNWurWPI4BYy/C7j501iP8ARm+veO3269pbCnMYPlRaZ/1Ypp2nv9mnd2MANAAFAOAA5ALZsepu/tizZ50GQx0ogytqNLS4lokaDqaNQ4tc08iqXluLncaXpOmSv3rji+Tjb60vGtLfciI855v2jBby4iG6e0U8dzA4u7SYpWs+RQ43XJ0jyzji3r/utolztuNvOsXmvq/vhjtNl7r3Hmoclu0thtYKFlm0tOpoNdAawuDWk+8SdX0ecfG7ndZYybnpWPD3dPv8XrJyO322KabfrafH3/t0dCy9vLcYm9t4W6pZbeWONtQKucwgCpoOZWi3FJtitWO81n2KDBaK5KzPaJj2qx5Ybfy+ExN3b5O3+HlluPEY3Wx9W6GitWOcOYVXwm0y4Mdq5I0mber0epZczusefJWcc6xEev0+tcldKdQtvbYzlp5h5PL3Ftox9x8R4M/iRnVre0t7ocXCoHSFn9psc1N7fLaPgnzaTrHp+1fbve4r7KmOs/HGnTSfQl9+bPi3Fi6RBrclbgutJTwr1xuP3XfIVM5Xjo3OPp+evb8ETjOQnbZOv5J7/i+bE/8AVcGN+A3BaGN9sALe6MsUmtnLS7Q9x1N6+kfK4v8Ama4/Jmrp5e06xOv2Scn/AC9r+fDbXXvGkx7lnVorBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEH//Z'></div><ul class='menu'><li><a href="https://gitee.com/zhucheer/orange" target="_blank">源码</a></li><li><a href="https://www.kancloud.cn/chase688/orange_framework" target="_blank">文档</a></li></ul></body></html>`