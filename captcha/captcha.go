package captcha

import (
	"gitee.com/zhucheer/orange/app"
	"gitee.com/zhucheer/orange/utils"
)

var captchaSessionName = "ORANGE-CAPTCHA"

func init() {
	ranTag := utils.ShortTag(utils.GetRandStr(2), 1)
	captchaSessionName += ranTag
}

func CaptchaImgShow(c *app.Context, n int, size ...int) error {
	text := utils.GetRandStr(n)
	textMd5 := utils.Md5ToString(text)

	width := 180
	height := 60
	if len(size) >= 2 {
		width = size[0]
		height = size[1]
	}

	c.Session().Set(captchaSessionName, textMd5)
	imgByte := ImgText(width, height, text)

	c.ResponseHeader().Set("Content-Type", "image/png")
	return c.ResponseWrite(imgByte)
}

func CaptchaVerify(c *app.Context, code string)bool {
	codeMd5:=utils.Md5ToString(code)
	sessionCode:= c.Session().Get(captchaSessionName)

	if codeMd5 == sessionCode{
		return true
	}
	return false
}
