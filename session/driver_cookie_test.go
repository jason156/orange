package session

import (
	"fmt"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"net/url"
	"testing"
)

func TestSessByCookie(t *testing.T) {

	var globalSessions *Manager

	httpServer := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		fmt.Println("test http api")

		config := &ManagerConfig{
			CookieName:      "orange",
			Gclifetime:      1800,
			DisableHTTPOnly: true,
			ProviderConfig:  "{}",
		}

		globalSessions, _ = NewManager("cookie", config)
		sid, _ := globalSessions.sessionID()

		sess, _ := globalSessions.SessionStart(w, r)

		sess.Set("ss", "6666")

		fmt.Println(sess.Get("ss"))

		fmt.Println(sid)

	}))
	defer httpServer.Close()
	httpUrl, _ := url.Parse(httpServer.URL)

	res, err := http.Get(httpUrl.String())
	if err != nil {
		t.Error(err)
	}
	greeting, _ := ioutil.ReadAll(res.Body)
	res.Body.Close()

	fmt.Println(string(greeting))
}
