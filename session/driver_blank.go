package session

import (
	"net/http"
)

// BlankSessionStore 空session驱动 用于未开启session时相关操作，避免空指针异常
type BlankSessionStore struct {
}

func (st *BlankSessionStore) Set(key, value interface{}) (err error) {
	st.warningSession("Set")
	return
}

func (st *BlankSessionStore) Get(key interface{}) (value interface{}) {
	st.warningSession("Get")
	return
}

func (st *BlankSessionStore) Delete(key interface{}) (err error) {
	st.warningSession("Delete")
	return
}

func (st *BlankSessionStore) SessionID() (sid string) {
	st.warningSession("SessionID")
	return
}

func (st *BlankSessionStore) SessionRelease(w http.ResponseWriter) (err error) {
	st.warningSession("SessionRelease")
	return
}

func (st *BlankSessionStore) Flush() (err error) {
	st.warningSession("Flush")
	return
}

func (st *BlankSessionStore) warningSession(funcName string) (err error) {
	//logger.Warning("session is not open, %s func is nothing to do, please check config.toml", funcName)
	return
}
