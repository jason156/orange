package logger

import (
	"fmt"
	"gitee.com/zhucheer/orange/cfg"
	"strings"
)

var logFacede *Logger

// 初始化logger快速方法
func NewLogger() {
	loggerLevel := strings.ToUpper(cfg.Config.GetString("app.logger.level"))
	loggerType := cfg.Config.GetString("app.logger.type")
	loggerPath := cfg.Config.GetString("app.logger.path")
	loggerSyncInterval := cfg.Config.GetInt("app.logger.syncInterval")

	if loggerType != "json" && loggerType != "text" {
		loggerType = "text"
	}

	level := INFO
	switch loggerLevel {
	case "DEBUG":
		level = DEBUG
	case "INFO":
		level = INFO
	case "NOTICE":
		level = NOTICE
	case "WARNING":
		level = WARNING
	case "ERROR":
		level = ERROR
	case "CRITICAL":
		level = CRITICAL
	}

	logFacede = New(level, loggerType, loggerPath, loggerSyncInterval)

	fmt.Println(fmt.Sprintf("[ORANGE] \033[0;33m start logger level[%v] type[%v] syncInterval[%v] \033[0m ",
		LevelString[level], loggerType, loggerSyncInterval))
}

// LogFile, new log file save.
func LogFile(fileName string) *Logger {
	checkLogNil()
	return logFacede.LogFile(fileName)
}

// Debug, record DEBUG message.
func Debug(format string, a ...interface{}) {
	checkLogNil()
	logFacede.Debug(format, a...)
}

// Info, record INFO message.
func Info(format string, a ...interface{}) {
	checkLogNil()
	logFacede.Info(format, a...)
}

// Notice, record INFO message.
func Notice(format string, a ...interface{}) {
	checkLogNil()
	logFacede.Notice(format, a...)
}

// Warning, record WARNING message.
func Warning(format string, a ...interface{}) {
	checkLogNil()
	logFacede.Warning(format, a...)
}

// Error, record ERROR message.
func Error(format string, a ...interface{}) {
	checkLogNil()
	logFacede.Error(format, a...)
}

// Critical, record CRITICAL message.
func Critical(format string, a ...interface{}) {
	checkLogNil()
	logFacede.Critical(format, a...)
}

// LogIsStart, check is open.
func LogIsStart() bool {
	if logFacede == nil {
		return false
	}
	return true
}

// checkLogNil check log should not empty
func checkLogNil() {
	if logFacede == nil {
		panic("log is nil, init log first")
	}
}
