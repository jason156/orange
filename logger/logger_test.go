package logger

import (
	"fmt"
	"os"
	"testing"
	"time"
)

func TestLogDefault(t *testing.T) {
	logDir := "./"
	logFileName := "default.log"
	defer func() {
		os.Remove(logDir + logFileName)
	}()

	logger := New(DEBUG, JsonType, logDir, 0)
	logger.Debug("test log %s", "xxxx")

	if fileExists(logDir+logFileName) == false {
		t.Error("log write file error")
	}
}

func TestLogFile(t *testing.T) {
	logDir := "./"
	logFileName := "default.log"
	logFileName2 := "test01.log"
	defer func() {
		os.Remove(logDir + logFileName)
		os.Remove(logDir + logFileName2)
	}()

	logger := New(DEBUG, JsonType, logDir, 1000)
	logger.Debug("test log %s", "xxxx")
	logger.LogFile("test01").Info("test info log %s", "xxxxx")
	logger.LogFile("test01").Info("test2 info log %s", "xxxxx")

	time.Sleep(2 * time.Second)

	if fileExists(logDir+logFileName) == false {
		t.Error("log write file error")
	}
	if fileExists(logDir+logFileName2) == false {
		t.Error("log write file error")
	}
}

func TestLogConsole(t *testing.T) {
	logger := New(DEBUG, TextType, "", 1000)
	logger.Debug("test Debug")
	logger.Info("test Info")
	logger.Notice("test Notice")
	logger.Warning("test Warning")
	logger.Error("test Error")
	logger.Critical("test Critical")

	logger.LogFile("test02").Info("test02 info log %s", "xxxxx")
	logger.LogFile("test02").Error("test02 error log %s", "xxxxx")

	time.Sleep(2 * time.Second)

}

func TestMoreLogFile(t *testing.T) {
	logger := New(DEBUG, TextType, "", 1000)

	for i := 1; i < 20; i++ {
		logFileName := fmt.Sprintf("test_%d", i)
		logger.LogFile(logFileName).Info("test info log %s", logFileName)
	}

	time.Sleep(2 * time.Second)
}

//func TestLogNil(t *testing.T) {
//	logFacede = nil
//
//	defer func() {
//		err := recover()
//		if err == nil {
//			t.Error("checkLogNil func have an err")
//		}
//	}()
//
//	Debug("test log nil")
//
//}

//func TestLogFacede(t *testing.T) {
//
//	logFacede = New(DEBUG, JsonType, "", 1)
//
//	Debug("test Debug")
//	Info("test Info")
//	Notice("test Notice")
//	Warning("test Warning")
//	Error("test Error")
//	Critical("test Critical")
//
//	time.Sleep(1 * time.Second)
//
//}

// 判断所给路径文件/文件夹是否存在
func fileExists(path string) bool {
	_, err := os.Stat(path) //os.Stat获取文件信息
	if err != nil {
		if os.IsExist(err) {
			return true
		}
		return false
	}
	return true
}
