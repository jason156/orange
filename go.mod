module gitee.com/zhucheer/orange

go 1.12

require (
	gitee.com/zhucheer/cfg v0.0.4
	github.com/fogleman/gg v1.3.0
	github.com/golang/freetype v0.0.0-20170609003504-e2365dfdc4a0
	github.com/gomodule/redigo v2.0.0+incompatible
	github.com/jinzhu/gorm v1.9.11
	github.com/zhuCheer/pool v0.2.1
	golang.org/x/image v0.0.0-20191009234506-e7c1f5e7dbb8
)
