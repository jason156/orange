package http

import (
	"gitee.com/zhucheer/orange/app"
	"gitee.com/zhucheer/orange/project/http/controller"
	"gitee.com/zhucheer/orange/project/http/middleware"
)

type Route struct {
}

func (s *Route) ServeMux() {
	commonGp := app.NewRouter("")
	{
		commonGp.GET("/", func(ctx *app.Context) error {
			return ctx.ToJson(map[string]interface{}{"data": "orange framework"})
		})

		commonGp.ALL("/welcome", controller.Welcome)
		commonGp.ALL("/test", controller.Test)
		commonGp.GET("/appDefer", controller.AppDefer)

		commonGp.ALL("/upload", controller.Upload)

		commonGp.GET("/captcha", controller.Captcha)
		commonGp.GET("/verifyimg", controller.VerifyImg)

		commonGp.GET("/selectMysql", controller.SelectMySql)
		commonGp.GET("/selectRedis", controller.SelectRedis)
	}

	authGp := commonGp.GroupRouter("/auth", middleware.NewAuth())
	{
		authGp.ALL("/info", controller.AuthCheck)
		loginGp := authGp.GroupRouter("/auth/login", middleware.NewLogin())
		{
			loginGp.ALL("/info", controller.AuthCheck)
		}

	}

}
