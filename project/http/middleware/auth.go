package middleware

import (
	"errors"
	"fmt"
	"gitee.com/zhucheer/orange/app"
)

type Auth struct {
}

func NewAuth() *Auth {
	return &Auth{}
}

// Func implements Middleware interface.
func (w Auth) Func() app.MiddlewareFunc {
	return func(next app.HandlerFunc) app.HandlerFunc {
		return func(c *app.Context) error {

			fmt.Println("auth middleware")
			// 中间件处理逻辑
			if c.Request().Header.Get("auth") == "" {
				c.ResponseWrite([]byte("auth middleware break"))
				return errors.New("auth middleware break")
			}

			return next(c)
		}
	}
}
