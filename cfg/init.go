package cfg

import (
	"gitee.com/zhucheer/cfg"
)

var Config = cfg.New("")

func init() {
	NewFlagParam()
	SetStringFlag("config", "./config/config.toml", "config file path")

}

func StartConfig() {
	configPath := GetStringFlag("config")
	Config = cfg.New(configPath)

	if Config == nil {
		panic("config init failed")
	}

}
