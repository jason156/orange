package cfg

import (
	"flag"
	"sync"
)

var f *flagParam

type flagParam struct {
	mutex     sync.Mutex
	appParams map[string]interface{}
}

// NewFlagParam create flag
func NewFlagParam() *flagParam {
	f = &flagParam{
		appParams: make(map[string]interface{}),
	}
	return f
}

// SetStringFlag register a string params
func SetStringFlag(name string, value string, usage string) {
	paramVal := flag.String(name, value, usage)

	f.mutex.Lock()
	defer f.mutex.Unlock()
	f.appParams[name] = paramVal
}

// SetIntFlag  register a int params
func SetIntFlag(name string, value int, usage string) {
	paramVal := flag.Int(name, value, usage)

	f.mutex.Lock()
	defer f.mutex.Unlock()
	f.appParams[name] = paramVal

}

func GetIntFlag(name string) int {
	paramVal, inMap := f.appParams[name]
	if inMap == false {
		return 0
	}
	showVal, typeOk := paramVal.(*int)
	if typeOk == false {
		return 0
	}
	return *showVal
}

func GetStringFlag(name string) string {

	paramVal, inMap := f.appParams[name]
	if inMap == false {
		return ""
	}
	showVal, typeOk := paramVal.(*string)
	if typeOk == false {
		return ""
	}
	return *showVal
}

func ParseParams() {
	flag.Parse()
}
